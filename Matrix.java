public class Matrix {
    public static void main(String[] args) {

        int[][] matrix = new int[4][4];
        matrix[0][0] = 1;
        matrix[0][1] = 5;
        matrix[0][2] = 7;
        matrix[0][3] = 2;
        matrix[1][0] = 8;
        matrix[1][1] = 9;
        matrix[1][2] = 1;
        matrix[1][3] = 2;
        matrix[2][0] = 5;
        matrix[2][1] = 4;
        matrix[2][2] = 7;
        matrix[2][3] = -8;
        matrix[3][0] = 1;
        matrix[3][1] = -1;
        matrix[3][2] = 5;
        matrix[3][3] = 1;

        int sumOne = 0;
        for (int i = 0; i < 4/2; i++) {
            for (int j = 4/2; j < 4; j++) {
                sumOne += matrix[i][j];
            }
        }

        for (int i = 4/2; i < 4; i++) {
            for (int j = 0; j < 4/2; j++) {
                sumOne += matrix[i][j];
            }
        }
        int sumTwo=0;
        for (int i = 0; i<matrix.length; i++){
            sumTwo += matrix[i][i];
        }
        int sumThree = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = i; j < 4; j++) {
                sumThree += matrix[i][j];
            }
        }
        System.out.println("Сумма двух четвертей матрицы:" + sumOne);
        System.out.println("Сумма главной диагонали:" + sumTwo);
        System.out.println("Сумма половины матрицы, включая главную дигональ" + sumThree);
    }
}
